@echo off
echo.
echo [信息] 打包Python项目，生成exe文件。
echo.

%~d0
cd %~dp0
call pyinstaller --add-data="../img;img" --add-data="../libs;libs" --add-data="../conf;conf" --version-file ../conf/app_version_info.txt -F -w -n 索依框架修改器V4-20230316 -i ../img/favicon.ico  ../run.py

echo.
echo [信息] 打包完成。
echo.

pause