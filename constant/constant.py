# -*- coding: utf-8 -*-
# @Time : 2021/05/18
# @Author : ricky
# @File : constant.py
# @Software: vscode
"""
系统常量
"""
APP_VERSION_INT = 420220517
APP_VERSION_STRING = '4.6.4.20220517'
PYTHON_VERSION_STRING = '3.7.9'
WXPYTHON_VERSION_STRING = '4.1.0'
APP_BITMAP_TYPE_ICO = 'img/favicon.ico'
APP_WORK_DIRECTORY = 'current_work_directory'
# 禁止修改的目录 
FORBID_ALTER_DIR_TUPLE = ('target', 'node_modules')