# -*- coding: utf-8 -*-
# @Time : 2021/05/18
# @Author : ricky
# @File : run.py
# @Software: vscode
"""
启动类
"""
import wx
from ui import main
from spider import upgradespider
from loguru import logger

if __name__ == '__main__':
    """
    程序入口
    """
    logger.add('out.log')
    app = wx.App(False)
    frame = main.Main(None)
    frame.Show(True)
    try:
        # 检测更新
        # upgradespider.check()
        app.MainLoop()

    except Exception as err:
        logger.error('程序发生异常: {}', err)
        raise