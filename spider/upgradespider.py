# -*- coding: utf-8 -*-
# @Time : 2021/02/23
# @Author : ricky
# @File : upgradespider.py
# @Software: vscode
"""
爬取gitee，检测更新
"""
import threading
import requests
import wx
from bs4 import BeautifulSoup
from constant import constant
from loguru import logger


def _get(show_no_new_version_tip=False):
    """
    异步解析

    参数:
        show_no_new_version_tip (bool): 是否显示没有新版本提示，默认为False
    """
    url = 'https://gitee.com/lpf_project/common-tools/tags'
    wb_data = requests.get(url)
    soup = BeautifulSoup(wb_data.text, 'html.parser')
    tags = soup.select('div.tag-item-action.tag-name>a')
    is_has_new_version = False
    for tag in tags:
        version = ''.join(list(filter(str.isdigit, tag['title'])))
        if int(version) > constant.APP_VERSION_INT:
            is_has_new_version = True
            logger.info('检查更新结果：发现新的版本-{}', tag['title'])
            wx.MessageDialog(
                None, '发现新的版本【%s】，请前往gitee下载，\ngitee地址可以查看“关于我们”中的“软件官网”' %
                tag['title'], '版本升级提醒', wx.OK).ShowModal()
            break
    if not is_has_new_version:
        logger.info('检查更新结果：当前版本是最新版本!')
        if show_no_new_version_tip:
            wx.MessageDialog(None, '当前版本是最新版本!', '检测更新', wx.OK).ShowModal()


def check(show_no_new_version_tip=False):
    """
    检测更新

    参数:
        show_no_new_version_tip (bool): 是否显示没有新版本提示，默认为False
    """
    thread = threading.Thread(target=_get, args=(show_no_new_version_tip, ))
    thread.start()
