# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc
import wx.propgrid as pg

###########################################################################
## Class ConfigDialog
###########################################################################

class ConfigDialog ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"配置参数", pos = wx.DefaultPosition, size = wx.Size( 318,450 ), style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		b_sizer = wx.BoxSizer( wx.VERTICAL )

		self.m_property_manager = pg.PropertyGridManager(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.propgrid.PGMAN_DEFAULT_STYLE|wx.propgrid.PG_BOLD_MODIFIED|wx.propgrid.PG_DESCRIPTION|wx.propgrid.PG_SPLITTER_AUTO_CENTER)

		self.m_property_page = self.m_property_manager.AddPage( u"基础设置", wx.NullBitmap )
		self.m_property_item_enable = self.m_property_page.Append( pg.BoolProperty( u"是否启用配置", u"config_enable" ) )
		self.m_property_page.SetPropertyHelpString(self.m_property_item_enable, u"选择为True时参数生效，在修改框架时会使用配置的参数，否则不使用" )
		self.m_property_item_mysql_category = self.m_property_page.Append( pg.PropertyCategory( u"数据库配置", u"mysql_config" ) )
		self.m_property_item_database_ip = self.m_property_page.Append( pg.LongStringProperty( u"IP地址", u"database_ip" ) )
		self.m_property_page.SetPropertyHelpString(self.m_property_item_database_ip, u"mysql数据库的ip地址，本地也可以填写localhost\n或者127.0.0.1" )
		self.m_property_item_database_port = self.m_property_page.Append( pg.IntProperty( u"端口号", u"database_port", 3306) )
		self.m_property_page.SetPropertyHelpString(self.m_property_item_database_port, u"mysql数据库的端口号，mysql默认3306" )
		self.m_property_item_database_name = self.m_property_page.Append( pg.LongStringProperty( u"名称", u"database_name" ) )
		self.m_property_page.SetPropertyHelpString(self.m_property_item_database_name, u"mysql数据库名称" )
		self.m_property_item_database_username = self.m_property_page.Append( pg.LongStringProperty( u"账号", u"database_username" ) )
		self.m_property_page.SetPropertyHelpString(self.m_property_item_database_username, u"mysql数据库登录账号（存储时会加密，放心填写）" )
		self.m_property_item_database_password = self.m_property_page.Append( pg.LongStringProperty( u"密码", u"database_password" ) )
		self.m_property_page.SetPropertyHelpString(self.m_property_item_database_password, u"mysql数据库登录密码（存储时会加密，放心填写）" )
		self.m_property_item_redis_category = self.m_property_page.Append( pg.PropertyCategory( u"Redis配置", u"redis_config" ) )
		self.m_property_item_redis_ip = self.m_property_page.Append( pg.LongStringProperty( u"IP地址", u"redis_ip" ) )
		self.m_property_page.SetPropertyHelpString(self.m_property_item_redis_ip, u"redis数据库的ip地址，本地也可以填写localhost\n或者127.0.0.1" )
		self.m_property_item_redis_port = self.m_property_page.Append( pg.IntProperty( u"端口号", u"redis_port", 6379 ) )
		self.m_property_page.SetPropertyHelpString(self.m_property_item_redis_port, u"redis数据库端口号，默认6379" )
		self.m_property_item_redis_password = self.m_property_page.Append( pg.LongStringProperty( u"密码", u"redis_password" ) )
		self.m_property_page.SetPropertyHelpString(self.m_property_item_redis_password, u"redis数据库登录密码（存储时会加密，放心填写）" )
		b_sizer.Add( self.m_property_manager, 1, wx.ALL|wx.EXPAND, 5 )

		self.m_static_text = wx.StaticText( self, wx.ID_ANY, u"注：设置一次后下次修改不需要再次设置", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_static_text.Wrap( -1 )

		b_sizer.Add( self.m_static_text, 0, wx.ALL, 5 )

		b_sizer2 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button_save = wx.Button( self, wx.ID_ANY, u"保存", wx.DefaultPosition, wx.DefaultSize, 0 )
		b_sizer2.Add( self.m_button_save, 0, wx.ALL, 5 )

		self.m_button_close = wx.Button( self, wx.ID_CANCEL, u"关闭", wx.DefaultPosition, wx.DefaultSize, 0 )
		b_sizer2.Add( self.m_button_close, 0, wx.ALL, 5 )


		b_sizer.Add( b_sizer2, 0, wx.ALIGN_RIGHT, 5 )

		self.SetSizer( b_sizer )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_button_save.Bind( wx.EVT_BUTTON, self.OnClickEventSave )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnClickEventSave( self, event ):
		event.Skip()


