# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class DruidEncryptDialog
###########################################################################

class DruidEncryptDialog ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"druid秘钥生成工具", pos = wx.DefaultPosition, size = wx.Size( 600,460 ), style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		b_sizer_main = wx.BoxSizer( wx.VERTICAL )

		self.m_scrolled_window = wx.ScrolledWindow( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.VSCROLL )
		self.m_scrolled_window.SetScrollRate( 0, 20 )
		b_sizer_scrolled = wx.BoxSizer( wx.VERTICAL )

		sb_sizer_original_password = wx.StaticBoxSizer( wx.StaticBox( self.m_scrolled_window, wx.ID_ANY, u"原始密码" ), wx.VERTICAL )

		b_sizer_original_password = wx.BoxSizer( wx.HORIZONTAL )

		self.m_text_ctrl_original_password = wx.TextCtrl( sb_sizer_original_password.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_text_ctrl_original_password.SetMaxLength( 32 )
		b_sizer_original_password.Add( self.m_text_ctrl_original_password, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.m_button_generate = wx.Button( sb_sizer_original_password.GetStaticBox(), wx.ID_ANY, u"生成", wx.DefaultPosition, wx.DefaultSize, 0 )
		b_sizer_original_password.Add( self.m_button_generate, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.m_button_clear = wx.Button( sb_sizer_original_password.GetStaticBox(), wx.ID_ANY, u"清空", wx.DefaultPosition, wx.DefaultSize, 0 )
		b_sizer_original_password.Add( self.m_button_clear, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )


		sb_sizer_original_password.Add( b_sizer_original_password, 0, wx.EXPAND, 5 )


		b_sizer_scrolled.Add( sb_sizer_original_password, 0, wx.ALL|wx.EXPAND, 5 )

		sb_sizer_private_key = wx.StaticBoxSizer( wx.StaticBox( self.m_scrolled_window, wx.ID_ANY, u"私钥" ), wx.VERTICAL )

		self.m_text_ctrl_private_key = wx.TextCtrl( sb_sizer_private_key.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY|wx.TE_WORDWRAP )
		self.m_text_ctrl_private_key.SetMinSize( wx.Size( -1,120 ) )

		sb_sizer_private_key.Add( self.m_text_ctrl_private_key, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_button_copy_private_key = wx.Button( sb_sizer_private_key.GetStaticBox(), wx.ID_ANY, u"点击复制", wx.DefaultPosition, wx.DefaultSize, 0 )
		sb_sizer_private_key.Add( self.m_button_copy_private_key, 0, wx.ALL, 5 )


		b_sizer_scrolled.Add( sb_sizer_private_key, 0, wx.ALL|wx.EXPAND, 5 )

		sb_sizer_public_key = wx.StaticBoxSizer( wx.StaticBox( self.m_scrolled_window, wx.ID_ANY, u"公钥" ), wx.VERTICAL )

		self.m_text_ctrl_public_key = wx.TextCtrl( sb_sizer_public_key.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY|wx.TE_WORDWRAP )
		self.m_text_ctrl_public_key.SetMinSize( wx.Size( -1,60 ) )

		sb_sizer_public_key.Add( self.m_text_ctrl_public_key, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_button_copy_public_key = wx.Button( sb_sizer_public_key.GetStaticBox(), wx.ID_ANY, u"点击复制", wx.DefaultPosition, wx.DefaultSize, 0 )
		sb_sizer_public_key.Add( self.m_button_copy_public_key, 0, wx.ALL, 5 )


		b_sizer_scrolled.Add( sb_sizer_public_key, 0, wx.ALL|wx.EXPAND, 5 )

		sb_sizer_encrypt_password = wx.StaticBoxSizer( wx.StaticBox( self.m_scrolled_window, wx.ID_ANY, u"密码" ), wx.VERTICAL )

		self.m_text_ctrl_encrypt_password = wx.TextCtrl( sb_sizer_encrypt_password.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY|wx.TE_WORDWRAP )
		self.m_text_ctrl_encrypt_password.SetMinSize( wx.Size( -1,60 ) )

		sb_sizer_encrypt_password.Add( self.m_text_ctrl_encrypt_password, 0, wx.ALL|wx.EXPAND, 5 )

		self.m_button_copy_encrypt_password = wx.Button( sb_sizer_encrypt_password.GetStaticBox(), wx.ID_ANY, u"点击复制", wx.DefaultPosition, wx.DefaultSize, 0 )
		sb_sizer_encrypt_password.Add( self.m_button_copy_encrypt_password, 0, wx.ALL, 5 )


		b_sizer_scrolled.Add( sb_sizer_encrypt_password, 0, wx.ALL|wx.EXPAND, 5 )


		self.m_scrolled_window.SetSizer( b_sizer_scrolled )
		self.m_scrolled_window.Layout()
		b_sizer_scrolled.Fit( self.m_scrolled_window )
		b_sizer_main.Add( self.m_scrolled_window, 1, wx.EXPAND |wx.ALL, 5 )


		self.SetSizer( b_sizer_main )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_button_generate.Bind( wx.EVT_BUTTON, self.OnClickEventGenerate )
		self.m_button_clear.Bind( wx.EVT_BUTTON, self.OnClickEventClear )
		self.m_button_copy_private_key.Bind( wx.EVT_BUTTON, self.OnClickEventPrivateKeyCopy )
		self.m_button_copy_public_key.Bind( wx.EVT_BUTTON, self.OnClickEventPublicKeyCopy )
		self.m_button_copy_encrypt_password.Bind( wx.EVT_BUTTON, self.OnClickEventEncryptPasswordCopy )

	def __del__( self ):
		pass


	# Virtual event handlers, overide them in your derived class
	def OnClickEventGenerate( self, event ):
		event.Skip()

	def OnClickEventClear( self, event ):
		event.Skip()

	def OnClickEventPrivateKeyCopy( self, event ):
		event.Skip()

	def OnClickEventPublicKeyCopy( self, event ):
		event.Skip()

	def OnClickEventEncryptPasswordCopy( self, event ):
		event.Skip()
