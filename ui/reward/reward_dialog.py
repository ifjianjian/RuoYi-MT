# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version Oct 26 2018)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class RewardDialog
###########################################################################

class RewardDialog ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"打赏作者", pos = wx.DefaultPosition, size = wx.Size( 600,381 ), style = wx.DEFAULT_DIALOG_STYLE )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )
		self.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )

		bSizer = wx.BoxSizer( wx.VERTICAL )

		self.m_bitmap_wx = wx.StaticBitmap( self, wx.ID_ANY, wx.NullBitmap, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_bitmap_wx.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )
		bSizer.Add( self.m_bitmap_wx, 1, wx.ALL|wx.EXPAND, 0 )

		self.m_static_line2 = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
		bSizer.Add( self.m_static_line2, 0, wx.EXPAND |wx.ALL, 0 )

		bSizer3 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_button_ok = wx.Button( self, wx.ID_ANY, u"赏你了！", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer3.Add( self.m_button_ok, 0, wx.ALL, 5 )

		self.m_button_no = wx.Button( self, wx.ID_ANY, u"不富裕，不打赏了", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer3.Add( self.m_button_no, 0, wx.ALL, 5 )


		bSizer.Add( bSizer3, 0, wx.ALIGN_RIGHT, 5 )


		self.SetSizer( bSizer )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.OnCancel )
		self.m_button_ok.Bind( wx.EVT_BUTTON, self.OnOk )
		self.m_button_no.Bind( wx.EVT_BUTTON, self.OnCancel )

	def __del__( self ):
		pass

	# Virtual event handlers, overide them in your derived class
	def OnOk( self, event ):
		event.Skip()
	
	def OnCancel( self, event ):
		event.Skip()

